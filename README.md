# ppwtk1

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

## Daftar isi

- [Daftar isi](#daftar-isi)
- [Nama-nama Anggota](#nama-nama-anggota)
- [Link Herokuapp](#link-herokuapp)
- [Cerita Aplikasi](#cerita-aplikasi)
- [Daftar Fitur](#daftar-fitur)

## Nama-nama Anggota:

- Antonius Anggito Arissaputro (1906292912)
- Bintang Dominique (1906292963)
- Budiman Arbenta (1906285535)
- Dewangga Putra Sheradhien (1906293013)

## Link Herokuapp:

- Link Website: http://jadwalin-aja.herokuapp.com/

## Cerita Aplikasi:

   Ide aplikasi ini berawal dari pengalaman kami selama menjalani kuliah secara online. Di sela-sela kegiatan belajar tentunya ada saatnya diperlukan waktu untuk berkumpul, baik untuk diskusi kelompok, rapat kepengurusan, atau sekedar berbincang dan menghabiskan waktu bersama (main Among Us misalnya). Karena tidak dapat bertemu di kondisi corona seperti ini, kami juga sulit menentukan waktu yang pas dengan jadwal masing-masing sehingga dibutuhkan suatu media untuk menentukan jadwal yang paling cocok dengan kami.

   Aplikasi ini berfungsi sebagai media untuk membantu sekelompok orang atau organisasi dalam menentukan jadwal berkumpul yang paling cocok berdasarkan jadwal kegiatan masing-masing pesertanya.

## Daftar Fitur:

   1. Fitur User

   2. Fitur Dashboard dan Feedback

   3. Fitur Jadwal

   4. Fitur Komentar

[pipeline-badge]: https://gitlab.com/ppw-b06/ppw-tk-1/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/ppw-b06/ppw-tk-1/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/ppw-b06/ppw-tk-1/-/commits/master