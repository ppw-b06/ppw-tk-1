from django.urls import path
from . import views
 
app_name = 'umpanbalik'

urlpatterns = [
    path('list', views.list, name='list'),
    path('create', views.create, name='create'),
    path('edit/<str:id>', views.edit, name='edit'),
]