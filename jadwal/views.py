from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import JadwalForm, JoinForm, IsiJadwalForm
from .models import Jadwal, Tanggal, Kegiatan
from userhandle.models import MyUser
from babel.dates import format_date
import string, secrets, datetime

BASE64 = []
TIMESTAMPS = []

def init():
    global BASE64
    global TIMESTAMPS
    
    BASE64 = list(string.ascii_uppercase + string.ascii_lowercase + string.digits + '-' + '_')
    
    for i in range(8, 22):
        TIMESTAMPS.append(datetime.time(hour=i).strftime('%H:%M'))

def date_range(awal, akhir):
    for day_count in range(int((akhir - awal).days) + 1):
        yield awal + datetime.timedelta(day_count)
        
init()

### BUAT JADWAL ###
def buat_jadwal(request):
    if (not request.user.is_authenticated):
        return HttpResponseRedirect('/userhandle/login')
    context = {}
    
    jadwal_form = JadwalForm(request.POST or None)
    
    if request.method == "POST":
        if jadwal_form.is_valid():
            owner = MyUser.objects.get(username=request.user)
            if owner:
                if owner.is_active:
                    ''' PROCESS JADWAL '''
                    jadwal = jadwal_form.save(commit=False)
                    
                    jadwal.owner_id = owner.id
                    
                    random = secrets.SystemRandom()
                    while True:
                        try:
                            url = ''.join(random.sample(BASE64, 40))
                            Jadwal.objects.get(url=url)
                        except Jadwal.DoesNotExist:
                            break
                    jadwal.url = url
                    
                    jadwal.save()
                    jadwal.users.add(owner)

                    ''' PROCESS TANGGAL & KEGIATAN '''     
                    dates = []
                    awal = jadwal.tanggal_awal
                    akhir = jadwal.tanggal_akhir
                    for date in date_range(awal, akhir):
                        tanggal = Tanggal(jadwal=jadwal, tanggal=date) 
                        tanggal.save()
                        
                        for hour in range(8, 21):
                            kegiatan = Kegiatan(user=owner, jadwal=jadwal, tanggal=tanggal, jam_awal=datetime.time(hour=hour))
                            kegiatan.save()
                    
                    return HttpResponseRedirect("/jadwal/" + url)
        else:
            context['jadwal_form'] = jadwal_form
    else:
        context['jadwal_form'] = JadwalForm()
    
    return render(request, 'jadwal/buat-jadwal.html', context)
 
### INVITE ### 
def invite(request, url):
    context = {}
    
    jadwal = Jadwal.objects.get(url=url)
    context['jadwal'] = jadwal      
    
    if request.method == "POST":
        if not request.user.is_authenticated:
            return HttpResponseRedirect('/userhandle/login')
        user = request.user
        join = JoinForm(request.POST)
        if join.is_valid():
            try:
                jadwal.users.get(username=user.username)
            except MyUser.DoesNotExist:
                if user.id != jadwal.owner_id:
                    jadwal.users.add(user)
                    
                    awal = jadwal.tanggal_awal
                    akhir = jadwal.tanggal_akhir
                    for date in date_range(awal, akhir):
                        tanggal = Tanggal.objects.get(jadwal=jadwal, tanggal=date)
                        
                        for hour in range(8, 21):
                            kegiatan = Kegiatan(user=user, jadwal=jadwal, tanggal=tanggal, jam_awal=datetime.time(hour=hour))
                            kegiatan.save()
            return HttpResponseRedirect("/jadwal/" + jadwal.url)
            
    context['join'] = JoinForm()
        
    return render(request, 'jadwal/invite.html', context)

### JADWAL ###
def jadwal(request, url):
    context = {}

    context['TS'] = TIMESTAMPS
    
    jadwal = Jadwal.objects.get(url=url)
    context['jadwal'] = jadwal

    if (not request.user.is_authenticated) or (request.user not in jadwal.users.all()):
        return HttpResponseRedirect('/')
    
    owner = MyUser.objects.get(id=jadwal.owner_id)
    context['owner'] = owner
    
    users = []
    for user in jadwal.users.all():
        if user != owner:
            users.append(user)
    context['users'] = users
    
    user = MyUser.objects.get(username=request.user)
    if user:
        if user.is_active:
            dates = dict()
            for date in Tanggal.objects.filter(jadwal=jadwal):
                tanggal = format_date(date.tanggal, "EEEE, dd/MM/yy", locale="id")
                kegiatan_count = []
                for hour in range(8, 21):
                    kegiatan_count.append(Kegiatan.objects.filter(jadwal=jadwal, tanggal=date, bisa=True, jam_awal=datetime.time(hour=hour)).count())
                dates[tanggal] = kegiatan_count
            context['dates'] = dates
            
    return render(request, 'jadwal/jadwal.html', context)
  
### ISI JADWAL ###  
def isi_jadwal(request, url):
    context = {}
    
    context['TS'] = TIMESTAMPS
    
    jadwal = Jadwal.objects.get(url=url)
    context['jadwal'] = jadwal
    
    if (not request.user.is_authenticated) or (request.user not in jadwal.users.all()):
        return HttpResponseRedirect('/')
    
    owner = MyUser.objects.get(id=jadwal.owner_id)
    context['owner'] = owner
    
    users = []
    for user in jadwal.users.all():
        if user != owner:
            users.append(user)
    context['users'] = users
    
    user = MyUser.objects.get(username=request.user)
    if user:
        if user.is_active:
            dates = dict()
            for date in Tanggal.objects.filter(jadwal=jadwal):
                tanggal = format_date(date.tanggal, "EEEE, dd/MM/yy", locale="id")
                kegiatan = []
                for hour in range(8, 21):
                    kegiatan_count = Kegiatan.objects.filter(jadwal=jadwal, tanggal=date, bisa=True, jam_awal=datetime.time(hour=hour)).count()
                    my_kegiatan = Kegiatan.objects.get(user=user, jadwal=jadwal, tanggal=date, jam_awal=datetime.time(hour=hour))
                    
                    form_prefix = str(my_kegiatan.id)
                    my_kegiatan_form = IsiJadwalForm(prefix=form_prefix, initial={'kegiatan_id' : my_kegiatan.id, 'bisa' : my_kegiatan.bisa})
                    
                    kegiatan.append((kegiatan_count, my_kegiatan_form))
                dates[tanggal] = kegiatan
            context['dates'] = dates
    
    if request.method == "POST":
        for key, value in request.POST.items():
            args = key.split('-')
            if len(args) > 1 and args[1] == "kegiatan_id":
                kegiatan_id = int(args[0])
                if request.POST.get(args[0] + "-bisa", False):
                    Kegiatan.objects.filter(id=kegiatan_id).update(bisa=True)
                else:
                    Kegiatan.objects.filter(id=kegiatan_id).update(bisa=False)
                    
        return HttpResponseRedirect(reverse('jadwal:jadwal', args=[jadwal.url]))
            
    return render(request, 'jadwal/isi-jadwal.html', context)
