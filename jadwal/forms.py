from django import forms
from django.core.exceptions import ValidationError
from jadwal.models import Jadwal, Kegiatan

class JadwalForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = ['judul', 'tanggal_awal', 'tanggal_akhir']
        
    judul = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                    'class' : 'form-input',
                    'placeholder' : 'Judul Rapat',
                    'required' : 'required'
                }
    ))
    
    tanggal_awal = forms.DateField(label="", input_formats=["%d/%m/%Y"], widget=forms.DateInput(
        attrs = {
                    'type' : 'text',
                    'class' : 'form-input',
                    'placeholder' : 'Tanggal awal',
                    'required' : 'required',
                    'onfocus' : 'dateInputFocus(this)',
                    'onblur' : 'dateInputBlur(this)'
                }
    ))
    
    tanggal_akhir = forms.DateField(label="", input_formats=["%d/%m/%Y"], widget=forms.DateInput(
        attrs = {
                    'type' : 'text',
                    'class' : 'form-input',
                    'placeholder' : 'Tanggal akhir',
                    'required' : 'required',
                    'onfocus' : 'dateInputFocus(this)',
                    'onblur' : 'dateInputBlur(this)'
                }
    ))
    
    def validate(self):
        cleaned_data = super().clean()
        tanggal_awal = cleaned_data.get("tanggal_awal")
        tanggal_akhir = cleaned_data.get("tanggal_akhir")

        if tanggal_awal and tanggal_akhir:
            if tanggal_awal > tanggal_akhir:
                raise ValidationError("Tanggal awal tidak boleh mulai setelah Tanggal akhir")
    
class AnggotaForm(forms.Form):
    nama = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                    'class' : 'form-input',
                    'placeholder' : 'Tambah Anggota'
                }
    ))

class JoinForm(forms.Form):
    join = forms.BooleanField(label="", initial=True, widget=forms.HiddenInput(attrs = {'readonly' : True}))
    
class IsiJadwalForm(forms.Form):
    kegiatan_id = forms.IntegerField(label="", widget=forms.HiddenInput())
    bisa = forms.BooleanField(label="", widget=forms.CheckboxInput(attrs = {'hidden' : True}))