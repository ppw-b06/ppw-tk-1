from django.db import models
from django.core.exceptions import ValidationError
from userhandle.models import MyUser

class Jadwal(models.Model):
    judul = models.CharField(max_length=50)
    url = models.CharField(editable=False, max_length=40)
    tanggal_awal = models.DateField()
    tanggal_akhir = models.DateField()
    owner_id = models.IntegerField()
    users = models.ManyToManyField(MyUser)
    
    def clean(self):
        if self.tanggal_awal and self.tanggal_akhir:
            if self.tanggal_awal > self.tanggal_akhir:
                raise ValidationError("Tanggal awal tidak boleh mulai setelah Tanggal akhir")
    
    def __str__(self):
        return self.judul
                
class Tanggal(models.Model):
    jadwal = models.ForeignKey('Jadwal', on_delete=models.CASCADE)
    tanggal = models.DateField()
    
class Kegiatan(models.Model):
    user = models.ForeignKey('userhandle.MyUser', on_delete=models.CASCADE)
    jadwal = models.ForeignKey('Jadwal', on_delete=models.CASCADE)
    tanggal = models.ForeignKey('Tanggal', on_delete=models.CASCADE)
    bisa = models.BooleanField(default=False)
    jam_awal = models.TimeField()