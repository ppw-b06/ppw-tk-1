from django.test import TestCase, Client
from userhandle.models import MyUser

class JadwalTestCases(TestCase):
    # def test_url_to_jadwal_exist(self):
    #     response = Client().get('/jadwal/')
        
    #     self.assertEqual(response.status_code, 200)
        
    def test_url_to_buat_jadwal_exist_tanpa_login(self):
        response = Client().get('/jadwal/buat-jadwal')
        
        self.assertEqual(response.status_code, 302)

    def test_url_to_buat_jadwal_exist_setelah_login(self):
        user = MyUser.objects.create(username='test', nama_panjang='TestTest')
        user.set_password('test123')
        user.save()
        self.client.login(username="test", password="test123")
        response = self.client.get('/jadwal/buat-jadwal')
        self.assertEqual(response.status_code, 200)
