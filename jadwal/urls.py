from django.urls import path
from . import views

app_name = 'jadwal'

urlpatterns = [
    path('buat-jadwal', views.buat_jadwal, name='buat-jadwal'),
    path('isi-jadwal/<str:url>', views.isi_jadwal, name='isi-jadwal'),
    path('invite/<str:url>', views.invite, name='invite'),
    path('<str:url>', views.jadwal, name='jadwal'),
]
