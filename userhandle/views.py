from django.shortcuts import render,redirect, HttpResponseRedirect
from django.http import HttpResponseBadRequest
from django.http import HttpResponse
from userhandle.admin import UserCreationForm, AuthenticationForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as django_login, authenticate as django_authenticate, logout as django_logout
from django.urls import reverse
from userhandle.models import MyUser

def login(request):
    messages = []
    if request.method == 'POST':
        form = AuthenticationForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = django_authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    django_login(request,user)
                    return HttpResponseRedirect(reverse('main:home')) # TODO: supposed to be redirected to dashboard
            messages.append('username or password does not match any account')
        else:
            if not form.username:
                messages.append('Please fill username')
            if not form.password:
                messages.append('Please fill password')
    else:
        form = AuthenticationForm()

    return render(request, 'userhandle/login.html', {'form':form, 'messages':messages})

def register(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.set_password(form.cleaned_data['password1'])            
            user.save()
            
            django_login(request,user)
            return HttpResponseRedirect(reverse('main:home')) # TODO: supposed to be redirected to dashboard
        else :
            messages.warning(request, form.errors)
    else:
        form = UserCreationForm()

    return render(request, "userhandle/register.html", {"form" : form})

@login_required
def logout(request):
    django_logout(request)
    return HttpResponseRedirect(reverse('main:home'))