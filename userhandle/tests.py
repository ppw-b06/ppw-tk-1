from django.test import TestCase, Client

from userhandle.models import MyUser

# use this to run coverage on local
# coverage run --parallel --include="userhandle/*" --omit="manage.py,ppwtk1/*" manage.py test --exclude-tag=functional
# coverage report -m

class TestUserHandle (TestCase):
    def test_apakah_url_login_ada(self):
        response = Client().get('/userhandle/login')
        self.assertEquals(response.status_code, 200)
        
    def test_apakah_url_register_ada(self):
        response = Client().get('/userhandle/register')
        self.assertEquals(response.status_code, 200)

    def test_apakah_url_logout_ada(self):
        response = Client().get('/userhandle/logout')
        self.assertEquals(response.status_code, 302)

    def test_apakah_di_login_sudah_ada_formulir_untuk_login(self):
        response = Client().get('/userhandle/login')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Log In", html_kembalian)
        
    def test_apakah_di_register_sudah_ada_formulir_untuk_register(self):
        response = Client().get('/userhandle/register')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Buat Akun", html_kembalian)
        self.assertIn("Register", html_kembalian)

    def test_apakah_model_MyUser_ada(self):
        MyUser.objects.create(username = "username_tes", nama_panjang = "nama_panjang_tes")
        hitung_user = MyUser.objects.all().count()
        self.assertEquals(hitung_user, 1)

    def test_apakah_sudah_bisa_buat_akun(self):
        response = Client().post('/userhandle/register', {
            'username' : 'aku123',
            'password1' : 'susahbanget142',
            'password2' : 'susahbanget142',
            'nama_panjang' : 'namapanjangku',
            })
        self.assertEquals(response.status_code, 302)
        hitung_user = MyUser.objects.all().count()
        self.assertEquals(hitung_user, 1)

    def test_apakah_saat_password1_berbeda_dengan_password2_akun_gagal_dibuat(self):
        response = Client().post('/userhandle/register', {
            'username' : 'aku123',
            'password1' : 'susahbanget142',
            'password2' : 'susahbanget143',
            'nama_panjang' : 'namapanjangku',
            })
        self.assertEquals(response.status_code, 200)
        hitung_user = MyUser.objects.all().count()
        self.assertEquals(hitung_user, 0)

    def test_apakah_saat_input_data_tidak_lengkap_akun_gagal_dibuat(self):
        response = Client().post('/userhandle/register', {
            'username' : '',
            'password1' : 'susahbanget142',
            'password2' : 'susahbanget142',
            'nama_panjang' : 'namapanjangku',
            })
        self.assertEquals(response.status_code, 200)
        response = Client().post('/userhandle/register', {
            'username' : 'aku123',
            'password1' : '',
            'password2' : 'susahbanget142',
            'nama_panjang' : 'namapanjangku',
            })
        self.assertEquals(response.status_code, 200)
        response = Client().post('/userhandle/register', {
            'username' : 'aku123',
            'password1' : 'susahbanget142',
            'password2' : '',
            'nama_panjang' : 'namapanjangku',
            })
        self.assertEquals(response.status_code, 200)
        response = Client().post('/userhandle/register', {
            'username' : 'aku123',
            'password1' : 'susahbanget142',
            'password2' : 'susahbanget142',
            'nama_panjang' : '',
            })
        self.assertEquals(response.status_code, 200)
        hitung_user = MyUser.objects.all().count()
        self.assertEquals(hitung_user, 0)
