from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from userhandle.models import MyUser

class AuthenticationForm(forms.Form): # Note: forms.Form NOT forms.ModelForm
    username = forms.SlugField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "username_sign",
                "class" : "form-input",
                "placeholder" : "Username",
                }
    ))
    password = forms.CharField(label='', widget=forms.PasswordInput(
        attrs = {
                "id" : "password_sign",
                "class" : "form-input",
                "placeholder" : "Password",
                "autocomplete" : "true",
                }
    ))

    class Meta:
        fields = ['username', 'password']

class UserCreationForm(forms.ModelForm):
    username = forms.SlugField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "username_sign",
                "class" : "form-input",
                "placeholder" : "Username",
                }
    ))

    password1 = forms.CharField(label='', widget=forms.PasswordInput(
        attrs = {
                "id" : "pass_1",
                "class" : "form-input",
                "placeholder" : "Password",
                }
    ))

    password2 = forms.CharField(label='', widget=forms.PasswordInput(
        attrs = {
                "id" : "pass_2",
                "class" : "form-input",
                "placeholder" : "Konfirmasi Password",
                }
    ))

    nama_panjang = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "nama_panjang_sign",
                "class" : "form-input",
                "placeholder" : "Nama Panjang",
                }
    ))

    class Meta:
        model = MyUser
        fields = ('username','password1', 'password2', 'nama_panjang')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class UserAdmin(BaseUserAdmin):
    form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('username', 'password1', 'password2', 'nama_panjang')}),
        ('Permissions', {'fields': ('is_admin',)}),
    )

    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', 'nama_panjang')}
        ),
    )
    search_fields = ('username',)
    ordering = ('username',)
    filter_horizontal = ()

# Now register the new UserAdmin...
admin.site.register(MyUser, UserAdmin)

# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)