from django.urls import path
from . import views

app_name = 'komentar'

urlpatterns = [
    path('<str:judul>', views.index_komentar, name='index_komentar'),
    path('tambah_komentar/<str:judul>/<str:name>', views.tambah_komentar, name='tambah_komentar'),
]
