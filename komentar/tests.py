from django.test import TestCase
from .models import Komentar
from datetime import datetime
from userhandle.models import MyUser
from jadwal.models import Jadwal

# Create your tests here.
# coverage run --include="komentar/*" --omit="main,manage.py,ppwtk1" manage.py test
# coverage run --include="dashboard/*,jadwal/*,komentar/*,main/*,umpanbalik/*,userhandle/*" --omit="manage.py,ppwtk1" manage.py test

class KomentarTestCase(TestCase):
    def test_komentar_is_exist_if_user_is_logged_in(self):
        user = MyUser.objects.create(username='test', nama_panjang='TestTest')
        user.set_password('test123')
        user.save()
        jadwal = Jadwal.objects.create(judul="bermain", tanggal_akhir=datetime.now(), tanggal_awal=datetime.now(), owner_id=user.pk)
        jadwal.users.add(user)
        self.client.login(username="test", password="test123")
        response = self.client.get('/komentar/' + jadwal.judul)
        self.assertIn('TestTest', response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'komentar/komentar.html')

    def test_komentar_redirects_if_user_is_not_logged_in(self):
        user = MyUser.objects.create(username='test', nama_panjang='TestTest')
        user.set_password('test123')
        user.save()
        jadwal = Jadwal.objects.create(judul="bermain", tanggal_akhir=datetime.now(), tanggal_awal=datetime.now(), owner_id=user.pk)
        response = self.client.get('/komentar/' + jadwal.judul)
        self.assertEqual(response.status_code, 302)

    def test_tambah_komentar_is_successful(self):
        user = MyUser.objects.create(username='test', nama_panjang='TestTest')
        user.set_password('test123')
        user.save()
        jadwal = Jadwal.objects.create(judul="bermain", tanggal_akhir=datetime.now(), tanggal_awal=datetime.now(), owner_id=user.pk)
        jadwal.users.add(user)
        self.client.login(username="test", password="test123")
        response = self.client.post('/komentar/tambah_komentar/' + jadwal.judul + '/' + user.nama_panjang, {'isi': 'Jadwal yang bagus!', 'jadwal': jadwal})
        self.assertEqual(Komentar.objects.all().count(), 1)
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/komentar/' + jadwal.judul)
        self.assertIn('Jadwal yang bagus!', response.content.decode('utf8'))

    def test_tambah_komentar_fails_if_user_is_not_logged_in(self):
        user = MyUser.objects.create(username='test', nama_panjang='TestTest')
        user.set_password('test123')
        user.save()
        jadwal = Jadwal.objects.create(judul="bermain", tanggal_akhir=datetime.now(), tanggal_awal=datetime.now(), owner_id=user.pk)
        jadwal.users.add(user)
        response = self.client.post('/komentar/tambah_komentar/' + jadwal.judul + '/' + user.nama_panjang, {'isi': 'Jadwal yang bagus!', 'jadwal': jadwal})
        self.assertEqual(Komentar.objects.all().count(), 0)
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/komentar/')
        self.assertNotIn('Jadwal yang bagus!', response.content.decode('utf8'))