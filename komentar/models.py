from django.db import models
from jadwal.models import Jadwal

class Komentar(models.Model):
    nama = models.CharField(max_length=30)
    tanggal = models.DateTimeField(auto_now_add=True, blank=True)
    isi = models.TextField()
    jadwal = models.ForeignKey(Jadwal, on_delete=models.CASCADE)

    def __str__(self):
        return self.isi