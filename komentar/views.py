from .forms import Komen_Form
from .models import Komentar
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from jadwal.models import Jadwal

def index_komentar(request, judul):
    jadwal = Jadwal.objects.get(judul=judul)
    if (not request.user.is_authenticated) or (request.user not in jadwal.users.all()):
        return redirect('main:home')

    response = {
        'komen_form': Komen_Form,
        'jadwal': jadwal
    }
    if Komentar.objects.all() is not None:
        response['daftar_komentar'] = Komentar.objects.all()
    return render(request, 'komentar/komentar.html', response)

def tambah_komentar(request, judul, name):
    jadwal = Jadwal.objects.get(judul=judul)
    if (not request.user.is_authenticated) or (request.user not in jadwal.users.all()):
        return redirect('main:home')

    form = Komen_Form(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        Komentar.objects.create(nama=name, isi=request.POST['isi'], jadwal = jadwal)
    return HttpResponseRedirect('/komentar/' + judul)