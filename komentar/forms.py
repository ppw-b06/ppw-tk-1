from django import forms
from .models import Komentar

class Komen_Form(forms.ModelForm):
    class Meta:
        model = Komentar
        fields = ['isi']

        labels = {
            'isi': ''
        }

        widgets = {
            'isi': forms.Textarea(attrs={'placeholder': 'Masukkan komentar anda di sini . . .'})
        }
        